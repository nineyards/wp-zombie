

	ZOMBIE

	By by Nine Yards

	a fork of STARKERS by Viewport Industries, Ltd.

	- - - - - - - - - - - - - - - - - - - - - - -

	Zombie is a bare-bones WordPress theme
	created to act as a starting point for the
	theme designer. Zombie includes Twitter Bootstrap
	and has Sass support

	Se readme.md for information on how to get started

	- - - - - - - - - - - - - - - - - - - - - - -

	Version 1.0, Initail release

	Forked Starkers and added Bootstrap, Sass & Grunt

	- - - - - - - - - - - - - - - - - - - - - - -

	Stay stoned!

	~ Adam Rehal

