**ZOMBIE**

A Wordpress starter theme by Nine Yards

Version 1.0

Zombie is a bare-bones WordPress theme 
created to act as a starting point for the 
theme designer. Zombie includes Twitter Bootstrap
and has Sass support 

**Getting started**

`cd wordpress/theme/directory`

Clone this repository: `git clone git@bitbucket.org:nineyards/wp-zombie`

Install dependecies: `npm install`

**Note**

This theme is included in WP-template. When starting a new WordPress project, use WP-template instead.
